package com.xingchen.workspaces;

import com.sun.jna.Library;

public interface Test extends Library{

	int add(int a, int b);  

	void addArray(int[] a, int[] b, int[] c, int len);

	String stringFun(String str1, String str2);

}

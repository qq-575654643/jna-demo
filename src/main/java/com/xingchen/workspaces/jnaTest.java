package com.xingchen.workspaces;

import com.sun.jna.Library;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import java.io.File;


public class jnaTest {

    public static String getDllBySystem() {
        //系统 Windows 或者 Linux
        String osName = System.getProperties().getProperty("os.name").toLowerCase();
        //架构 x86 或者 amd64
        String osArch = System.getProperties().getProperty("os.arch").toLowerCase();
        System.out.println("This OS is: "+osName+";"+osArch);
        String fileName = null;
        if(osArch.indexOf("64")!=-1){//64位
            if(osName.indexOf("win")!=-1){
                fileName = "test_64.dll";
            }else{
                fileName = "test_64.so";
            }
            System.out.println(System.getProperty("user.dir") + File.separator + fileName);
        }else if(osArch.indexOf("86")!=-1){//32位
            if(osName.indexOf("win")!=-1){
                fileName = "test_32.dll";
            }else{
                fileName = "test_32.so";
            }
            System.out.println(System.getProperty("user.dir") + File.separator + fileName);
        }else{//不支持的
            System.out.println("This OS is not support！");
        }
        return fileName;
    }

    public static void main(String[] args) {
        String dllFilename = getDllBySystem();
        //调用
        Test lib = (Test) Native.load(dllFilename, Test.class);
        int result = lib.add(1, 2);
        System.out.println("result = " + result);
        System.out.println("hello world!");
    }
}
